const fs = require('fs');
const jison = require('jison');

const grammar = fs.readFileSync('src/grammar/grammar.jison', { encoding: 'utf-8' });
const parser = new jison.Parser(grammar);

const parserSource = parser.generate({ moduleName: 'bmdl' });
fs.writeFileSync('dist/parser.js', parserSource);
