const express = require('express');
const path = require('path');
const app = express();
const port = 3000;

app.use('/', express.static(path.resolve('./src/demo')));
app.use('/dist', express.static(path.resolve('./dist')));

app.listen(port, () => console.log(`Demo app listening on port ${port}!`));
