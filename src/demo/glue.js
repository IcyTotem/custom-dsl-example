'use strict';

function defineCustomSyntaxHighlighting () {
    CodeMirror.defineSimpleMode('simplemode', {
        start: [
            { regex: /\"[^"\n]+\"/, token: 'string' },
            { regex: /(?:section|song|end|measure|tempo|import|to|from|time_signature)\b/, token: 'keyword' },
            { regex: /[A-G](\#|b)?[1-8]\/(128|64|32|16|8|4|2|1)(\.{0,3})/, token: 'variable' },
            { regex: /rest\/(128|64|32|16|8|4|2|1)(\.{0,3})/, token: 'variable' },
            { regex: /\d+/i, token: 'number'},
            { regex: /\@[^\n\r]*/, token: 'comment' }
        ],
        comment: [
            { regex: /\@[^\n\r]*/, token: 'comment' }
        ],
        meta: {  lineComment: '@' }
    });
}

window.addEventListener('load', () => {
    const parseButton = document.querySelector('#parse-button');
    const sourceInput = document.querySelector('#song-source');
    const errorBar = document.querySelector('#error-bar');

    defineCustomSyntaxHighlighting();

    const editor = CodeMirror.fromTextArea(sourceInput, {
        lineNumbers: true,
        styleActiveLine: { nonEmpty: true },
        theme: 'oceanic-next'
    });

    const playingInfoArea = document.querySelector('#playing-info');
    const currentSectionNameSpan = document.querySelector('#current-section-name');
    const currentMeasureNumberSpan = document.querySelector('#current-measure-number');
    
    function displayError (e) {
        playingInfoArea.style.display = 'none';

        errorBar.style.display = 'block';
        errorBar.innerText = e.message || e.toString();

        // These fields are usually provided by the generated parser
        if (e.hash && e.hash.loc) {
            const loc = e.hash.loc;
            editor.focus();
            editor.setSelection(
                { line: loc.first_line, ch: loc.first_column }, 
                { line: loc.last_line, ch: loc.last_column });
        } else if (e.hash && e.hash.line) {
            editor.focus();
            editor.setCursor({ line: e.hash.line });
        }
    }

    function clearError () {
        errorBar.style.display = 'none';
        errorBar.innerText = '';
    }

    function updatePlayingInfo (currentSectionName, currentMeasureIndex) {
        playingInfoArea.style.display = 'block';
        currentSectionNameSpan.innerText = currentSectionName;
        currentMeasureNumberSpan.innerText = (currentMeasureIndex + 1).toString();
    }

    /**
     * Convert our note object into a note duration usable by Tone.js.
     * @param {Note} note 
     * @returns {string} A string in the format {length}n{.+}, where length is 1, 2, 4, 8, 16, 32, 64 or 128
     *     and up to three dot characters follow the middle "n".
     */
    function convertDurationForTone (note) {
        return note.duration.toFixed(0) + 'n' + '.'.repeat(note.dots || 0);
    }

    /**
     * Convert our note object into a note name usable by Tone.js.
     * @param {Note} note 
     * @returns {string} A string in the format {name}{alteration}{octave}.
     */
    function convertNameForTone (note) {
        return note.name + (note.alteration || '') + note.octave;
    }

    /**
     * If the given event has new tempo and/or time signatures, apply them to the Transport immediately.
     * @param {SequenceEvent} event 
     * @param {boolean} ramp If true, tempo will ramp up/down to the given value over 1 second,
     *     otherwise it will change instantly.
     */
    function applyEventUpdates (event, ramp) {
        if (event.newTempo && event.newTempo.unit === 'bpm') {
            if (ramp) {
                Tone.Transport.bpm.rampTo(event.newTempo.value, 1);
            } else {
                Tone.Transport.bpm.value = event.newTempo.value;
            }
        }

        if (event.newTimeSignature) {
            Tone.Transport.timeSignature = [
                event.newTimeSignature.numerator,
                event.newTimeSignature.denominator
            ];
        }
    }

    /**
     * Use Tone.js Transport to play a series of notes encoded by the output of the given sequencer,
     * using the default ugly synthetic membrane sound.
     * @param {MusicProgramSequencer} sequencer 
     */
    function play (sequencer) {
        const synth = new Tone.Synth().toMaster();

        // We will use the Transport to schedule each measure independently. Given that we
        // inform Tone.js of the current tempo and time signature, the Transport will be
        // able to automatically interpret all measures and note durations as absolute
        // time events in seconds without us actually bothering
        let measureCounter = 0;
        let firstEvent = true;

        // Stop, rewind and clear all events from the transport (from previous plays)
        Tone.Transport.stop();
        Tone.Transport.position = 0;
        Tone.Transport.cancel();
        
        for (const event of sequencer.sequence()) {
            // The first event is always supposed to have new tempo and time signature info
            // so we should update the Transport appropriately
            if (firstEvent) {
                applyEventUpdates(event, false);
                firstEvent = false;
            }

            // In the following callback, "time" represents the absolute time in seconds
            // that the measure we are scheduling is expected to begin at, given the current
            // tempo and time signature assigned to the Transport
            Tone.Transport.schedule((time) => {
                // Change the tempo if this event has a new tempo. Also do the same if a new time signatue is issued
                applyEventUpdates(event, true);

                // This contains the relative time of notes with respect to the
                // start of the current measure, in seconds
                let relativeTime = 0;

                // Update playing info displayed to the user
                updatePlayingInfo(event.originatingSectionName, event.measureIndexWithinOriginatingSection);

                for (const note of event.measure.notes) {
                    const duration = convertDurationForTone(note);

                    // If this is an actual note (as opposed to a rest), schedule the
                    // corresponding sound to be played along the Transport timeline
                    // after the previous notes in the measure have been played (gence the relativeTime)
                    if (note.type === 'note') {
                        const name = convertNameForTone(note);
                        synth.triggerAttackRelease(name, duration, time + relativeTime);
                    }

                    // This is used to delay notes that come next by the correct amount
                    relativeTime += Tone.Time(duration).toSeconds();

                }
            }, `${measureCounter}m`);

            measureCounter++;
        }
        
        Tone.Transport.start();
    }

    parseButton.addEventListener('click', () => {
        // The object "bmdl" is defined in the global scope and contains the main parser
        const parser = bmdl;
        const source = editor.getValue() + '\n';

        try {
            const program = parser.parse(source);
            const verifier = new MusicProgramVerifier(program);
            const verifiedProgram = verifier.verify();
            const sequencer = new MusicProgramSequencer(verifiedProgram);
            play(sequencer);
            clearError();
        } catch (e) {
            displayError(e);
        }
    });
});