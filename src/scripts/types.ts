interface Note {
    type: 'note',
    name: 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G',
    alteration: '#' | 'b',
    octave: number,
    duration: number,
    dots: number
}

interface Rest {
    type: 'rest',
    duration: number,
    dots: number
}

type MusicUnit = Note | Rest;

interface StaticMeasure {
    definition: 'static',
    notes: MusicUnit[]
}

interface ImportedMeasure {
    definition: 'import',
    from: number,
    to: number,
    source: string
}

type Measure = StaticMeasure | ImportedMeasure;

interface Tempo {
    value: number,
    unit: 'bpm'
}

interface TimeSignature {
    numerator: number,
    denominator: number
}

interface Meta {
    tempo: Tempo,
    timeSignature: TimeSignature
}

interface Section {
    name: string,
    attributes: Meta,
    measures: Measure[]
}

interface Song {
    name: string,
    sections?: string[]
}

interface MusicProgram {
    sections?: Section[],
    song: Song
}

export {
    MusicProgram,
    Song,
    Section,
    Measure,
    StaticMeasure,
    ImportedMeasure,
    Note,
    Rest,
    Tempo,
    TimeSignature
};