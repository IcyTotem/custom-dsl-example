import { Section, StaticMeasure, Tempo, TimeSignature } from './types';
import { VerifiedMusicProgram } from './verifier';

interface SequenceEvent {
    measure: StaticMeasure;
    measureIndexWithinOriginatingSection: number;
    originatingSectionName: string;
    newTempo?: Tempo;
    newTimeSignature?: TimeSignature;
}

class MusicProgramSequencer {
    private program: VerifiedMusicProgram;
    private currentTempo: Tempo = null;
    private currentTimeSignature: TimeSignature = null;

    public constructor (program: VerifiedMusicProgram) {
        if (!program.verified) {
            throw new Error('Expcting a verified music program');
        }

        this.program = program;
    }

    public *sequence () : Iterable<SequenceEvent> {
        this.currentTempo = null;
        this.currentTimeSignature = null;

        for (const section of this.orderedSections()) {
            let measureCounter = 0;

            for (const measure of this.interpolateMeasures(section)) {
                const event: SequenceEvent = { 
                    measure,
                    originatingSectionName: section.name,
                    measureIndexWithinOriginatingSection: measureCounter++
                };

                if (this.isNewTempo(section.attributes.tempo)) {
                    event.newTempo = section.attributes.tempo;
                }

                if (this.isNewTimeSignature(section.attributes.timeSignature)) {
                    event.newTimeSignature = section.attributes.timeSignature;
                }

                yield event;

                this.currentTempo = section.attributes.tempo;
                this.currentTimeSignature = section.attributes.timeSignature;
            }
        }
    }

    private *orderedSections () : Iterable<Section> {
        for (const name of this.program.song.sections) {
            yield this.program.sectionMap[name];
        }
    }

    private *interpolateMeasures (section: Section) : Iterable<StaticMeasure> {
        for (const measure of section.measures) {
            if (measure.definition === 'static') {
                yield measure;
            } else {
                const importedSection = this.program.sectionMap[measure.source];
                for (const soughtMeasure of this.seekMeasures(importedSection, measure.from, measure.to)) {
                    yield soughtMeasure;
                }
            }
        }
    }

    private *seekMeasures (section: Section, from: number, to: number) : Iterable<StaticMeasure> {
        for (let i = from; i <= to; ++i) {
            const measure = section.measures[i - 1];
            if (measure.definition === 'static') {
                yield measure;
            }
        }
    }

    private isNewTempo (newTempo: Tempo) : boolean {
        const oldTempo = this.currentTempo;
        return (oldTempo === null) 
            || (oldTempo.value !== newTempo.value) 
            || (oldTempo.unit != newTempo.unit);
    }

    private isNewTimeSignature (newTimeSignature: TimeSignature) : boolean {
        const oldTimeSignature = this.currentTimeSignature;
        return (oldTimeSignature === null)
            || (oldTimeSignature.numerator !== newTimeSignature.numerator)
            || (oldTimeSignature.denominator !== newTimeSignature.denominator);
    }
}

export { MusicProgramSequencer, SequenceEvent };