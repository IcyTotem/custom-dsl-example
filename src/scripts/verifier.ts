import { MusicProgram, Section, StaticMeasure, ImportedMeasure } from './types';

interface VerifiedMusicProgram extends MusicProgram {
    verified: boolean;
    sectionMap: { [key: string]: Section };
    sections: Section[]
}

class MusicProgramVerifier {
    private definedSectionNames: string[];
    private sectionMap: { [key: string]: Section };
    private program: MusicProgram;

    public constructor (program: MusicProgram) {
        this.program = program;
        this.program.sections = this.program.sections || [];
        
        this.definedSectionNames = this.program.sections.map(section => section.name);

        this.sectionMap = { };
        this.program.sections.forEach(section => this.sectionMap[section.name] = section);
    }

    public verify () : VerifiedMusicProgram {
        this.verifyNotEmpty();
        this.verifyUsedSectionsAreDefined();
        this.verifyImportedSectionsAreDefined();
        this.verifySectionsMeasureDuration();
        this.verifyImportedMeasuresHaveValidNumbers();

        return {
            ...this.program,
            sectionMap: this.sectionMap,
            verified: true
        } as VerifiedMusicProgram;
    }

    private verifyNotEmpty() {
        if (this.program.sections.length === 0) {
            throw new Error('Your song does not contain any music');
        }
    }

    private verifyUsedSectionsAreDefined () {
        const undefinedSection = this.program.song.sections.find(name => !this.isSectionDefined(name));

        if (undefinedSection) {
            throw new Error(`Section "${undefinedSection}" has not been defined`);
        }
    }

    private verifyImportedSectionsAreDefined () {
        this.scanAllImportedMeasures((s, i, m) => this.verifyImportedMeasureSourceIsDefined(s, i, m));
    }

    private verifyImportedMeasureSourceIsDefined (section: Section, index: number, measure: ImportedMeasure) {
        if (!this.isSectionDefined(measure.source)) {
            throw new Error(`Measure ${index + 1} of section "${section.name}" is importing measures from "${measure.source}", which is not defined`);
        }
    }

    private isSectionDefined (name: string) : boolean {
        return this.definedSectionNames.includes(name);
    }

    private verifySectionsMeasureDuration () {
        this.program.sections.forEach(section => this.verifyMeasuresDuration(section));
    }

    private verifyMeasuresDuration (section: Section) {
        const timeSignature = section.attributes.timeSignature;
        const definedUnits = timeSignature.numerator * this.getUnits(timeSignature.denominator);

        for (let i = 0; i < section.measures.length; ++i) {
            const measure = section.measures[i];
            if (measure.definition !== 'static') {
                continue;
            }

            const measureUnits = this.getMeasureUnits(measure);
            if (measureUnits > definedUnits) {
                throw new Error(`Measure ${i + 1} of section "${section.name}" is longer than ${timeSignature.numerator}/${timeSignature.denominator}`);
            }
        }
    }

    private verifyImportedMeasuresHaveValidNumbers () {
        this.scanAllImportedMeasures((s, i, m) => this.verifyImportedMeasureNumbers(s, i, m));
    }

    private verifyImportedMeasureNumbers (section: Section, index: number, measure: ImportedMeasure) {
        const prefix = `Measure ${index + 1} of section "${section.name}"`;

        // Technically not possible since the minus symbol is not allowed in any lexeme
        if (measure.from <= 0 || measure.to <= 0) {
            throw new Error(`${prefix} has invalid negative numbers in its importing references`);
        }

        if (measure.to < measure.from) {
            throw new Error(`${prefix} has invalid numbers in its importing references becase "from" should be less than or equal to "to"`);
        }

        // Assume target section exists since it should have already been verified
        const targetSection = this.sectionMap[measure.source];

        if (measure.from > targetSection.measures.length || measure.to > targetSection.measures.length) {
            throw new Error(
                `${prefix} refers to measures ${measure.from} to ${measure.to} ` +
                `of section "${targetSection.name}", but the latter only has ${targetSection.measures.length} measures`);
        }

        // For simplicity, disallow importing from measures that themselves import other measures
        const doubleImport = targetSection.measures.find((m, index) => 
            (index + 1 >= measure.from) &&
            (index + 1 <= measure.to) &&
            (m.definition === 'import'));
        
        if (doubleImport) {
            throw new Error(`${prefix} is trying to import a range of measures that contains another import. Recursive imports are disallowed`);
        }
    }

    private scanAllImportedMeasures (callback: (section: Section, index: number, measure: ImportedMeasure) => void) {
        for (const section of this.program.sections) {
            section.measures.forEach((measure, index) => measure.definition === 'import' && callback(section, index, measure));
        }
    }

    private getMeasureUnits (measure: StaticMeasure) : number {
        let total = 0;

        for (const note of measure.notes) {
            const units = this.getUnits(note.duration);
            total += units;

            if (note.dots) {
                if (note.dots >= 1) total += units / 2;
                if (note.dots >= 2) total += units / 4;
                if (note.dots >= 3) total += units / 8;
            }
        }

        return total;
    }

    private getUnits (duration: number) : number {
        return 128 / duration;
    }
}

export { MusicProgramVerifier, VerifiedMusicProgram };