## What is this?

This is a toy project aimed at exemplifying the usage of parser generators to create custom *domain specific languages*. In this case, we are creating a very simplistic and naive language to define simple **music** pieces in terms of measures and notes, which should illustrate how the output of a compiled/interpreted source code may be any media type and not necessarily just a program.

## Running the example

Simply run:

    npm install
    npm run build
    npm test
    npm run start

If everything went well, you should have a running webserver listening on port 3000. It serves a single-page app with a coding area that can be filled in with a valid music program, according to the grammar defined in `src/grammar/grammar.jison`. Try this:

    section "Intro"
        tempo: 120 bpm
        time_signature: 2/4
        ---
        measure: rest/4 B4/16 A4/16 G#4/16 A4/16
        measure: C5/8 rest/8 D5/16 C5/16 B4/16 C5/16
        measure: E5/8 rest/8 F5/16 E5/16 D#5/16 E5/16
        measure: B5/16 A5/16 G#5/16 A5/16 B5/16 A5/16 G#5/16 A5/16
        measure: C6/4 A5/8 C6/8
        measure: B5/8 A5/8 G5/8 A5/8
        measure: B5/8 A5/8 G5/8 A5/8
        measure: B5/8 A5/8 G5/8 F#5/8
        measure: E5/4
        measure: import 1 to 9 from "Intro"
    end

    song "Rondo alla Turca"
        section "Intro"
    end