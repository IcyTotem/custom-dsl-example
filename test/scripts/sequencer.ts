import * as bmdl from '../../dist/parser';
import { assert } from 'chai';
import { describe, it } from 'mocha';
import { MusicProgramVerifier, VerifiedMusicProgram } from '../../src/scripts/verifier';
import { MusicProgram, Section } from '../../src/scripts/types';
import { MusicProgramSequencer } from '../../src/scripts/sequencer';

function parseAndVerifyProgramSource (source: string) : VerifiedMusicProgram {
    const program = bmdl.parse(source + '\n') as MusicProgram;
    const verifier = new MusicProgramVerifier(program);
    return verifier.verify();
}

function iterateToArray<T> (iterable: Iterable<T>) : T[] {
    const result: T[] = [];
    for (const item of iterable) {
        result.push(item);
    }
    return result;
}

describe('MusicProgramSequencer', () => {
    it('should produce a correct event sequence in a simple case', () => {
        const verifiedProgram = parseAndVerifyProgramSource(`
            section "First"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: A4/8 B4/8 B4/4
            end

            song "Test song"
                section "First"
            end`);
        
        const sequencer = new MusicProgramSequencer(verifiedProgram);
        const events = iterateToArray(sequencer.sequence());

        assert.lengthOf(events, 2);

        // Check generated values to be consistent
        assert.equal(events[0].originatingSectionName, 'First');
        assert.equal(events[0].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[1].originatingSectionName, 'First');
        assert.equal(events[1].measureIndexWithinOriginatingSection, 1);
        
        // First event has attributes
        assert.deepEqual(events[0].newTempo, {
            value: 95,
            unit: 'bpm'
        });
        assert.deepEqual(events[0].newTimeSignature, {
            numerator: 3,
            denominator: 4
        });

        // Second event doesn't
        assert.isNotOk(events[1].newTempo);
        assert.isNotOk(events[1].newTimeSignature);

        // Measures are reported correctly
        const onlySection = verifiedProgram.sections[0];
        assert.deepEqual(events[0].measure, onlySection.measures[0]);
        assert.deepEqual(events[1].measure, onlySection.measures[1]);
    });

    it('should report tempo and time signature changes', () => {
        const verifiedProgram = parseAndVerifyProgramSource(`
            section "First"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: A4/8 B4/8 B4/4
            end

            section "Second"
                tempo: 170 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: A4/8 B4/8 B4/4
            end

            section "Third"
                tempo: 170 bpm
                time_signature: 12/8
                ---
                measure: C#4/4 B4/4 rest/4
                measure: A4/8 B4/8 B4/4
            end

            song "Test song"
                section "First"
                section "Second"
                section "Third"
            end`);
        
        const sequencer = new MusicProgramSequencer(verifiedProgram);
        const events = iterateToArray(sequencer.sequence());

        assert.lengthOf(events, 6);

        // Check generated values to be consistent
        assert.equal(events[0].originatingSectionName, 'First');
        assert.equal(events[0].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[1].originatingSectionName, 'First');
        assert.equal(events[1].measureIndexWithinOriginatingSection, 1);
        assert.equal(events[2].originatingSectionName, 'Second');
        assert.equal(events[2].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[3].originatingSectionName, 'Second');
        assert.equal(events[3].measureIndexWithinOriginatingSection, 1);
        assert.equal(events[4].originatingSectionName, 'Third');
        assert.equal(events[4].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[5].originatingSectionName, 'Third');
        assert.equal(events[5].measureIndexWithinOriginatingSection, 1);

        // First event has both attributes
        assert.deepEqual(events[0].newTempo, {
            value: 95,
            unit: 'bpm'
        });
        assert.deepEqual(events[0].newTimeSignature, {
            numerator: 3,
            denominator: 4
        });

        // Third measure only changes tempo
        assert.isNotOk(events[2].newTimeSignature);
        assert.deepEqual(events[2].newTempo, {
            value: 170,
            unit: 'bpm'
        });
        
        // Fifth measure only changes time signature
        assert.isNotOk(events[4].newTempo);
        assert.deepEqual(events[4].newTimeSignature, {
            numerator: 12,
            denominator: 8
        });

        // All other events have no attribute changes
        for (const index of [1, 3, 5]) {
            assert.isNotOk(events[index].newTempo);
            assert.isNotOk(events[index].newTimeSignature);
        }

        // All measures produce events
        for (let i = 0; i < 6; ++i) {
            const s = Math.floor(i / 2);
            assert.deepEqual(
                events[i].measure, 
                verifiedProgram.sections[s].measures[i % 2]);
        }
    });

    it('should expand imported measures', () => {
        const verifiedProgram = parseAndVerifyProgramSource(`
            section "First"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: A4/8 B4/8 B4/4
            end

            section "Second"
                tempo: 170 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: import 1 to 2 from "First"
            end

            song "Test song"
                section "First"
                section "Second"
            end`);
        
        const sequencer = new MusicProgramSequencer(verifiedProgram);
        const events = iterateToArray(sequencer.sequence());

        assert.lengthOf(events, 5);

        assert.equal(events[0].originatingSectionName, 'First');
        assert.equal(events[0].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[1].originatingSectionName, 'First');
        assert.equal(events[1].measureIndexWithinOriginatingSection, 1);
        assert.equal(events[2].originatingSectionName, 'Second');
        assert.equal(events[2].measureIndexWithinOriginatingSection, 0);
        assert.equal(events[3].originatingSectionName, 'Second');
        assert.equal(events[3].measureIndexWithinOriginatingSection, 1);
        assert.equal(events[4].originatingSectionName, 'Second');
        assert.equal(events[4].measureIndexWithinOriginatingSection, 2);

        // Two measures from "First"
        assert.deepEqual(events[0].measure, verifiedProgram.sections[0].measures[0]);
        assert.deepEqual(events[1].measure, verifiedProgram.sections[0].measures[1]);
        
        // First measure from "Second"
        assert.deepEqual(events[2].measure, verifiedProgram.sections[1].measures[0]);
        
        // Then importing the first two measures from "First" again
        assert.deepEqual(events[3].measure, verifiedProgram.sections[0].measures[0]);
        assert.deepEqual(events[4].measure, verifiedProgram.sections[0].measures[1]);
    });
});