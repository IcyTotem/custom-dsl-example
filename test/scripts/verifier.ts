import * as bmdl from '../../dist/parser';
import { assert } from 'chai';
import { describe, it } from 'mocha';
import { MusicProgramVerifier } from '../../src/scripts/verifier';
import { MusicProgram, Section } from '../../src/scripts/types';

function parseProgramSource (source: string) : MusicProgram {
    return bmdl.parse(source + '\n') as MusicProgram;
}

describe('MusicProgramVerifier', () => {
    it('does not throw any errors if the program is correct', () => {
        const program = parseProgramSource(`
            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/16 rest/4
            end

            song "Test song"
                section "Andante ma non troppo"
            end`);
        
        const verifier = new MusicProgramVerifier(program);
        const result = verifier.verify();

        assert.isTrue(result.verified);
        assert.hasAllKeys(result.sectionMap, ['Andante ma non troppo']);
        assert.lengthOf(result.sections, 1);
    });

    it('throws an error if there are no defined sections', () => {
        const program = parseProgramSource(`
            song "Test song"
                section "First"
            end`);
        
        const verifier = new MusicProgramVerifier(program);
        assert.throws(
            () => verifier.verify(), 
            'Your song does not contain any music');
    });

    it('throws an error if a section is used but not defined', () => {
        const program = parseProgramSource(`
            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
            end

            song "Test song"
                section "Introduction"
            end`);
        
        const verifier = new MusicProgramVerifier(program);
        assert.throws(
            () => verifier.verify(), 
            'Section "Introduction" has not been defined');
    });

    it('throws an error if a measure is imported from a section that is not defined', () => {
        const program = parseProgramSource(`
            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: import 1 to 6 from "Missing"
            end

            song "Test song"
                section "Andante ma non troppo"
            end`);
        
        const verifier = new MusicProgramVerifier(program);
        assert.throws(
            () => verifier.verify(), 
            'Measure 1 of section "Andante ma non troppo" is importing measures from "Missing", which is not defined');
    });

    it('throws an error if a measure contains notes whose total duration exceeds the time signature', () => {
        const program = parseProgramSource(`
            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 6/8
                ---
                measure: A4/4. B4/8 C5/8 D5/8
                measure: C#4/4. B4/4. rest/4 D3/4
            end

            song "Test song"
                section "Andante ma non troppo"
            end`);
        
        const verifier = new MusicProgramVerifier(program);
        assert.throws(
            () => verifier.verify(), 
            'Measure 2 of section "Andante ma non troppo" is longer than 6/8');
    });

    describe('throws an error if the import reference numbers of a measure are invalid', () => {
        const createProgramFor = (from: number, to: number) => parseProgramSource(`
            section "First"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
            end

            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: C#4/4 B4/4 rest/4
                measure: import 1 to 1 from "First"
            end

            section "Second"
                tempo: 95 bpm
                time_signature: 3/4
                ---
                measure: import ${from} to ${to} from "Andante ma non troppo"
            end

            song "Test song"
                section "Andante ma non troppo"
            end`);
        
        const testCase = (from: number, to: number, errorMessage: string) => {
            const program = createProgramFor(from, to);
            const verifier = new MusicProgramVerifier(program);
            assert.throws(
                () => verifier.verify(), 
                errorMessage);
        };

        const prefix = 'Measure 1 of section "Second"';

        it('when to is less than from', () => {
            testCase(12, 10, `${prefix} has invalid numbers in its importing references becase "from" should be less than or equal to "to"`);
        });

        it('when they refer to measures that do not exist', () => {
            testCase(45, 78, `${prefix} refers to measures 45 to 78 of section "Andante ma non troppo", but the latter only has 2 measures`);
        });

        it('when they have recursive imports', () => {
            testCase(1, 2, `${prefix} is trying to import a range of measures that contains another import. Recursive imports are disallowed`);
        });
    })
});