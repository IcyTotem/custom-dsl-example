import * as bmdl from '../../dist/parser';
import { assert } from 'chai';
import { describe, it } from 'mocha';
import { MusicProgram } from '../../src/scripts/types';

function parseProgramSource (source: string) : MusicProgram {
    return bmdl.parse(source + '\n') as MusicProgram;
}

describe('Grammar', () => {
    it('produces a correct parser', () => {
        const program = parseProgramSource(`
            section "Andante ma non troppo"
                tempo: 95 bpm
                time_signature: 10/4
                ---
                @ This comment is ignored
                measure: A1/128 B2/64. C#3/32 Db4/16.. E5/8 F6/4 G7/2... A8/1
                measure: rest/128.. rest/64 rest/32. rest/16 rest/8... rest/4 rest/2 rest/1
            end

            song "Test song"
                section "Andante ma non troppo"
            end`);
        
        assert.deepEqual(program.song, {
            name: 'Test song',
            sections: ['Andante ma non troppo']
        });

        assert.deepEqual(program.sections[0].attributes, {
            tempo: {
                value: 95,
                unit: 'bpm'
            },
            timeSignature: {
                numerator: 10,
                denominator: 4
            }
        });

        assert.deepEqual(program.sections[0].measures[0], {
            definition: 'static',
            notes: [
                {
                    type: 'note',
                    name: 'A',
                    alteration: undefined,
                    duration: 128,
                    dots: 0,
                    octave: 1
                },
                {
                    type: 'note',
                    name: 'B',
                    alteration: undefined,
                    duration: 64,
                    dots: 1,
                    octave: 2
                },
                {
                    type: 'note',
                    name: 'C',
                    alteration: '#',
                    duration: 32,
                    dots: 0,
                    octave: 3
                },
                {
                    type: 'note',
                    name: 'D',
                    alteration: 'b',
                    duration: 16,
                    dots: 2,
                    octave: 4
                },
                {
                    type: 'note',
                    name: 'E',
                    alteration: undefined,
                    duration: 8,
                    dots: 0,
                    octave: 5
                },
                {
                    type: 'note',
                    name: 'F',
                    alteration: undefined,
                    duration: 4,
                    dots: 0,
                    octave: 6
                },   
                {
                    type: 'note',
                    name: 'G',
                    alteration: undefined,
                    duration: 2,
                    dots: 3,
                    octave: 7
                },
                {
                    type: 'note',
                    name: 'A',
                    alteration: undefined,
                    duration: 1,
                    dots: 0,
                    octave: 8
                }
            ]
        });

        assert.deepEqual(program.sections[0].measures[1], {
            definition: 'static',
            notes: [
                {
                    type: 'rest',
                    duration: 128,
                    dots: 2
                },
                {
                    type: 'rest',
                    duration: 64,
                    dots: 0
                },
                {
                    type: 'rest',
                    duration: 32,
                    dots: 1
                },
                {
                    type: 'rest',
                    duration: 16,
                    dots: 0
                },
                {
                    type: 'rest',
                    duration: 8,
                    dots: 3
                },
                {
                    type: 'rest',
                    duration: 4,
                    dots: 0
                },
                {
                    type: 'rest',
                    duration: 2,
                    dots: 0
                },
                {
                    type: 'rest',
                    duration: 1,
                    dots: 0
                }
            ]
        })
    });
});